from psycopg2 import pool
import psycopg2
import os

class handler:

    def __init__(self, params):

        self.params = params
        self.conn_pool = psycopg2.pool.ThreadedConnectionPool(5, 20, **params)

    def close(self):

        self.conn_pool.closeall()

    def execute(self, statement, values=None):

        conn = self.conn_pool.getconn()
        curs = conn.cursor()

        try:
            if values:
                curs.execute(statement, values)
            else:
                curs.execute(statement)

            conn.commit()

            return [c for c in curs.fetchall()]
        except Exception as e:
            print(f"Unable to execute statement:\n{statement}\n{e}")
            conn.rollback()
        finally:
            curs.close()
            self.conn_pool.putconn(conn)

    def version(self):

        return self.execute('SELECT Version();')[0][0]

    def update_api_info(self, url, version):
        statement = "SELECT cardano.update_api_version(%s,%s)"
        values = (url, version)
        self.execute(statement,values)

    def get_api_info(self):
        statement = """ SELECT * FROM cardano.blockfrost
                        WHERE id = (SELECT max(id) FROM cardano.blockfrost);"""
        return self.execute(statement)[0]

    




