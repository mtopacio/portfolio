from configparser import ConfigParser
from database import handler
from api import *

config = ConfigParser()
config.read('../config')
db_config = {k:v for k,v in config.items('POSTGRES')}

if __name__=="__main__":

    print("Starting main.py")
    #print(f"Uses: {get_usage()}")

    db = handler(db_config)
    #r = get_root()
    #db.update_api_info(r['url'], r['version'])

    pool_functions = [get_retiring_pools]

    page = 1

    for fun in pool_functions:

        end_of_results = False
        while not end_of_results:

            results = fun(page)
            print(len(results))

            for result in results:
                if fun == get_retiring_pools:
                    pool_id = result['pool_id']
                    epoch = result['epoch']
                    status = 'retiring'
                if fun == get_retired_pools:
                    pool_id = result['pool_id']
                    epoch = result['epoch']
                    status = 'retired'
                if fun == get_pools:
                    pool_id = result
                    status = 'active'
                    epoch = None

                stats = get_pool_stats(pool_id)
                print(stats)
                values = (pool_id, status, epoch)
                print(values)

            page+=1
            if len(results) == 0:
                end_of_results = True
