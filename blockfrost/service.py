from configparser import ConfigParser
from datetime import datetime
from fastapi import FastAPI
import requests
import json

config = ConfigParser()
config.read('./blockfrost/config')
base = config['BLOCKFROST']['ENDPOINT']
api_key = config['BLOCKFROST']['API_KEY']

# request header
headers = {'project_id':api_key}

app = FastAPI()

def req(extension):
    r = requests.get(f"{base}{extension}", headers=headers)
    return json.loads(r.text)

@app.get('/time')
async def server_time():
    res = req('/health/clock')
    servertime = datetime.fromtimestamp(int(res['server_time'])/1000)
    return servertime

@app.get('/pools')
async def pools():
    return req('/pools')

