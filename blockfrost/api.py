from configparser import ConfigParser
from datetime import datetime
import requests
import json

config = ConfigParser()
config.read('../config')
base = config['BLOCKFROST']['ENDPOINT']
api_key = config['BLOCKFROST']['API_KEY']

# request header
headers = {'project_id':api_key}

def req(extension='', params={}):
    r = requests.get(f"{base}{extension}", headers=headers, params=params)
    return json.loads(r.text)

""" API INTERFACE  """

def get_root():
    return req()

def get_usage():
    res = req('/metrics')
    for r in res:
        r['time'] = datetime.fromtimestamp(r['time'])
    return res

def get_time():
    res = req('/health/clock')
    servertime = datetime.fromtimestamp(int(res['server_time'])/1000)
    return servertime

def get_pools(page=None):
    params = {'count':100, 'page':page}
    return req('/pools', params)

def get_retired_pools(page=None):
    params = {'count':100, 'page':page}
    return req('/pools/retired', params)

def get_retiring_pools(page=None):
    params = {'count':100, 'page':page}
    return req('/pools/retiring', params)

def get_pool_stats(pool_id):
    return req(f'/pools/{pool_id}')

def get_pool_history(pool_id):
    return req(f'/pools/{pool_id}/history')

def get_pool_meta(pool_id):
    return req(f'/pools/{pool_id}/metadata')

def get_pool_relays(pool_id):
    return req(f'/pools/{pool_id}/relays')

def get_pool_delegators(pool_id):
    return req(f'/pools/{pool_id}/delegators')

def get_pool_blocks(pool_id):
    return req(f'/pools/{pool_id}/blocks')

def get_block(block_hash):
    return req(f'/blocks/{block_hash}')

def get_block_latest():
    return req('/blocks/latest')

def get_block_previous(block_hash, page=None):
    params = {'count':100, 'page':page}
    return req(f'/blocks/{block_hash}/previous', params)

def get_epochs_latest():
    return req('/epochs/latest')

def get_epochs_stakes(number, page=None):
    params = {'count':100, 'page':page}
    return req(f'/epochs/{number}/stakes',params)

def get_epochs_by_pool(number, pool_id, page=None):
    params = {'count':100, 'page':page}
    return req(f'/epochs/{number}/stakes/{pool_id}',params)

def get_epochs_by_block(number, pool_id, page=None):
    params = {'count':100, 'page':page}
    return req(f'/epochs/{number}/blocks/{pool_id}',params)

def get_genesis():
    return req(f'/genesis')

