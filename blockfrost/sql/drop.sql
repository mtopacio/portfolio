\c postgres

-- drops database and associations

DROP SCHEMA IF EXISTS cardano CASCADE;
DROP DATABASE IF EXISTS cardano;
DROP USER IF EXISTS ada;
