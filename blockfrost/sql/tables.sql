/*

Tables:
    - 


*/

-- general --

CREATE TABLE IF NOT EXISTS cardano.blockfrost (
    id INT GENERATED ALWAYS AS IDENTITY,
    url VARCHAR(50),
    version VARCHAR(50),
    PRIMARY KEY (url,version)
);

-- network --

CREATE TABLE IF NOT EXISTS cardano.network ( 
    date DATE DEFAULT CURRENT_TIMESTAMP PRIMARY KEY,
    max BIGINT NOT NULL,
    total BIGINT NOT NULL,
    circulating BIGINT NOT NULL,
    locked BIGINT NOT NULL,
    treasury BIGINT NOT NULL,
    reserves BIGINT NOT NULL,
    live BIGINT NOT NULL,
    active BIGINT NOT NULL
);

-- pools --

CREATE TABLE IF NOT EXISTS cardano.last_update (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS cardano.pools (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    pool_id VARCHAR(100) NOT NULL,
    status CARDANO.STATUS NOT NULL,
    epoch_retirement INT 
);

CREATE TABLE IF NOT EXISTS cardano.pool_stats (
    id INT PRIMARY KEY,
    blocks_minted INT NOT NULL,
    live_stake BIGINT NOT NULL,
    live_size NUMERIC NOT NULL,
    live_saturation NUMERIC NOT NULL,
    live_delegators SMALLINT NOT NULL,
    active_stake BIGINT NOT NULL,
    active_size NUMERIC NOT NULL,
    declared_pledge BIGINT NOT NULL,
    live_pledge BIGINT NOT NULL,
    margin_cost NUMERIC NOT NULL,
    fixed_cost BIGINT NOT NULL,
    reward_account VARCHAR(100) NOT NULL,
    owner VARCHAR(100) ARRAY NOT NULL,
    registration VARCHAR(100) ARRAY NOT NULL,
    retirement VARCHAR(100) ARRAY NOT NULL,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES cardano.pools(id)
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS cardano.pool_history (
    id INT PRIMARY KEY,
    epoch INT NOT NULL,
    blocks INT NOT NULL,
    delegators_count INT NOT NULL,
    rewards BIGINT NOT NULL,
    fees BIGINT NOT NULL,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES cardano.pools(id)
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS cardano.pool_metadata (
    id INT PRIMARY KEY,
    url TEXT NOT NULL,
    ticker VARCHAR(5) NOT NULL,
    name VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    homepage TEXT NOT NULL,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES cardano.pools(id)
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS cardano.pool_relays (
    id INT PRIMARY KEY,
    ipv4 TEXT,
    ipv6 TEXT,
    dns TEXT,
    dns_srv TEXT,
    city VARCHAR(50),
    country VARCHAR(50),
    port SMALLINT,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES cardano.pools(id)
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS cardano.delegators (
    id INT PRIMARY KEY,
    address VARCHAR(100) NOT NULL,
    live_stake BIGINT NOT NULL,
    CONSTRAINT fk_id FOREIGN KEY (id)
        REFERENCES cardano.pools(id)
        ON DELETE NO ACTION
);

-- epochs --

CREATE TABLE IF NOT EXISTS cardano.epochs (
    epoch_id SMALLINT PRIMARY KEY,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    first_block_time TIMESTAMP NOT NULL,
    last_block_time TIMESTAMP NOT NULL,
    block_count INT NOT NULL,
    tx_count INT NOT NULL,
    output BIGINT NOT NULL,
    fees BIGINT NOT NULL,
    active_stake BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS cardano.blocks (
    block_id VARCHAR(100) PRIMARY KEY,
    epoch_id SMALLINT NOT NULL,
    time TIMESTAMP NOT NULL,
    slot BIGINT NOT NULL,
    epoch_slot INT NOT NULL 
);



