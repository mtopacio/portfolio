/*

status: current status of pool

*/

CREATE TYPE cardano.status AS ENUM('active','retired','retiring');

