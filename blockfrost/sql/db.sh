#!/usr/bin/env bash

CURRENT_DIR=$(pwd)
SQL_DIR=${CURRENT_DIR}

# INSTALLATION SCRIPT

if [[ -z $1 ]]; then

    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

    sudo apt-get update
    sudo apt-get -y install postgresql

    su - postgres
    initdb --local=en_US.UTF-8 -E UTF8 -D /var/lib/postgres/data
    exit

    systemctl enable postgresql.service
    systemctl start postgresql.service

    echo Intializing database
    cd $SQL_DIR
    PGPASSWORD=postgres psql -U postgres -f $SQL_DIR/init.sql

elif [[ $1 == 'up' ]]; then
    
    echo Intializing database
    PGPASSWORD=postgres psql -U postgres -f $SQL_DIR/init.sql

elif [[ $1 == 'down' ]]; then

    echo Tearing down database
    PGPASSWORD=postgres psql -U postgres -f $SQL_DIR/drop.sql

fi



