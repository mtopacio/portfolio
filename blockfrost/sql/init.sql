CREATE DATABASE cardano;

\c cardano
DROP SCHEMA IF EXISTS public;
CREATE SCHEMA cardano;

\echo DDL scripts...
\echo

\ir enums.sql
\ir tables.sql
\ir functions.sql
\ir private-users.sql
\ir users.sql
