
-- creation of user ada in private sql file

GRANT CONNECT ON DATABASE cardano TO ada;
GRANT USAGE ON SCHEMA cardano TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.blockfrost TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.network TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.last_update TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.pools TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.pool_stats TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.pool_history TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.pool_metadata TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.pool_relays TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.delegators TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.epochs TO ada;
GRANT SELECT, INSERT, UPDATE ON TABLE cardano.blocks TO ada;
