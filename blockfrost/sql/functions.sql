CREATE OR REPLACE FUNCTION cardano.update_api_version(
        new_url VARCHAR,
        new_version VARCHAR)
    RETURNS VOID AS
    $$
    BEGIN
        INSERT INTO cardano.blockfrost(url,version)
            VALUES (new_url, new_version)
            ON CONFLICT (url,version) DO
                UPDATE SET url=new_url, version=new_version;
    END
    $$
    LANGUAGE plpgsql;

