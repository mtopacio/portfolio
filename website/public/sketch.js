let x, y, w, h;
let circ;
let circles = [];
let num_circles = 500;
let circle_diameters = 6;

function setup() {
  h = windowHeight;
  w = windowWidth;
  createCanvas(w, h);
 
  // create list of circles
  for (let i=0; i < num_circles; i++) {
    circles.push(new Circle())
  }
}

function draw() {
   background(0);
  for (let i=0; i < circles.length; i++) {
    let circ = circles[i];
    circ.move();
    circ.display();
  }
}

class Circle {
  constructor() {
    this.x = random(w);
    this.y = random(h);
    this.d = random(circle_diameters)  ;
    this.speed = random(1);
    this.alpha = 255;
    this.color = 255;
  }
  
  move() {
    
    // sparkle
    //this.alpha = (random(128) + 128 * sin(millis() / 100));
    this.alpha = random(50,255);
    fill(this.color, this.alpha);
    
    this.x += this.speed;
    this.y += this.speed;
    
    if (this.x >= w) {
      this.x = 0;
    }
    if (this.y >= h) {
      this.y = 0
    }
  }
  
  display() {
    ellipse(this.x, this.y, this.d, this.d);
  }
}