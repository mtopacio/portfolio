# website

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

---

Sections:
	About 
		- casual "resume"
	Resume 
		- formal resume
		- visualization of resume
	Notebooks
	Services
		- Data Analytics
			- Machine Learning
		- ETL
			- webscraping
			- parsing
			- converting filetypes
		- database analytics
			- 
